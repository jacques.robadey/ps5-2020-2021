---
version: 2
titre: îlots de chaleur en ville de Fribourg
type de projet: Projet de semestre 5
année scolaire: 2020/2021
mandants:
  - ville de Fribourg
  - projet de l’HEIA-FR smart living labs et FRI-LoRanet
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 2
professeurs co-superviseurs:
  - Serge Ayer
mots-clé: [IoT, Internet des objets, smart city, îlots de chaleur, capteurs connectés]
langue: [F, D]
confidentialité: non
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth]{img/ilot-urbain-chaleur.jpg}
\end{center}
```

## Contexte

Un nouveau projet d’analyse des îlots de chaleur urbains vient d’être lancé par la ville de Fribourg.
Ce projet de semestre a pour but d’implémenter un système de mesure sur une zone de 200 m2 avec des capteurs de température munis d’une interface LoRa (système de communication « Long Range » nécessitant très peu d’énergie) et de récolter les mesures via le réseau de télécommunication LoRa déployé en ville de Fribourg. Une interface de visualisation doit être développée pour pouvoir cartographier la température sur les zones mesurées et observer l’évolution du profil de température. Une analyse critique du système de mesure, du degré de couverture du réseau LoRa et de la qualité des mesures devra aussi être effectuée. Dans le cas d’un manque de couverture ou de précision des mesures, des solutions devront être proposées et si possible implémentées.


## Objectifs

De manière chronologique, le projet a les buts suivants :
1)	Faire une étude de l’art des solutions et résultats de mesure d’îlots de chaleur
2)	déterminer les endroits propices à l’apparition d’îlots de chaleur en ville de Fribourg
3)	tester les connectivités LoRa et dans un cas négatif trouver des solutions alternatives
4)	sélectionner et commander des capteurs de température LoRa et organiser le banc de test avec un vingtaine de capteurs distribués
5)	analyser les résultats de manière critique et proposer des améliorations si nécessaire


## Contraintes

Utilisation de capteurs LoRa et du réseau FRI-LoRanet, suivre les propositions/conseils du responsable du projet d’îlots de chaleur de la ville de (Marc Vonlathen)

