---
version: 2
titre: Un réseau qui s'autodimensionne en fonction du débit
type de projet: Projet de semestre 5
année scolaire: 2020/2021
mandants:
  - la filière ISC pour de nouveaux TP
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 2
professeurs co-superviseurs:
  - François Buntschu
mots-clé: [management de réseau, configuration de réseau, bande passante à la demande, protection]
langue: [F, D]
confidentialité: non
suite: non
---

## Contexte

Lors d’un récent travail de bachelor une plateforme optique d’un réseau national permettant la créer des lignes à la demande a été développé. Quatre nœuds forment le réseau avec un nombre de liens directs entre 2 noeuds s’adaptant au trafic en temps réel. Ce fut un succès mais le système de gestion du réseau peut encore être améliorée. Il faut entre autres changer la configuration uniquement lorsque c’est nécessaire et choisir la meilleure configuration pour tout changement. 

Ce travail de semestre est proposé dans ce but. Une version simplifiée du réseau national sera développée avec des petits switchs cisco. Ces derniers pourront simuler la création de lignes avec des lignes existantes qu’on active ou désactive.
Le générateur de trafic « spirent » sera programmé pour simuler un trafic fluctuant réel. De fort changements de trafic seront générés entre chaque nœud ce qui peut correspondre à des débuts et fins de backups entre serveurs. Des logiciels de gestion de réseau réactifs à ces changements de trafic seront développés et comparés. Le meilleur mécanisme de gestion sera défini en mesurant et comparant le trafic créé par le spirent et le trafic reçu par le destinataire.



## Objectifs

1)	Développer l’image d’un réseau national par un réseau de petits switchs avec multiples connexions de 1Gb/s.
2)	Définir différentes matrices de trafic et les programmer de manière séquentielle sur le générateur de trafic spirent
3)	Créer différents mécanismes/algorithmes de gestion des connexions, les tester et critiquer leurs avantages et inconvénients.
4)	Garder un trafic constant et simuler la rupture d’un lien.
5)	Tester la qualité de l’algorithme en contrôlant si trafic arrivant à destination diminue ou pas. Dans le cas de l’activation d’une ligne de protection, tester la réactivé cette protection en mesurant le temps nécessaire pour atteindre le débit originel.



## Contraintes

utilisation des équipements du labo comme le générateur de trafic spirent et les switches de labo cisco