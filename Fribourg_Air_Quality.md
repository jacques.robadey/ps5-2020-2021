---
version: 2
titre: La qualité de l'air de Fribourg cartographiée grâce à des capteurs de microparticules installés sur les bus des TPF
type de projet: Projet de semestre 5
année scolaire: 2020/2021
mandants:
  - ville de Fribourg
  - service de l'environnement
  - TPF
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 2
professeurs co-superviseurs:
  - Serge Ayer
mots-clé: [LoRa, Internet des objets, qualité de l’air, service de l’environnement, particules fines, plateforme de visualisation interactive]
langue: [F, D]
confidentialité: non
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth]{img/bus.jpg}
\end{center}
```

## Contexte

Les villes de Fribourg et Bulle sont en phase de devenir des villes intelligentes grâce au projet Fri-LoRanet qui a déployé un réseau LoRa dans les 2 villes afin de collecter des mesures de trafic routier, bruit et qualité de l’air. Si le trafic routier est déjà contrôlé, les mesures de la qualité de l’air ne sont pas encore opérationnelles.
Le but de ce travail de semestre est de les développer en utilisant des capteurs de particules fines et le réseau LoRa. Pour diminuer le nombre de capteurs, un accord a été conclu avec les TPF pour pouvoir installer les capteurs sur leur bus. La position du bus devra donc être mesurée en tout temps. Pour cela, un traqueur gps muni d’une interface LoRa devra être choisi et installé au côté du capteur de qualité de l’air. Les 2 capteurs transmettront leurs données via le réseau vers un serveur web. L’application du serveur stockant et permettant une visualisation géographique et historique des données de pollution devra être développée durant le projet. L’entreprise softcom, active dans le domaine des smart cities et partenaire du projet FRI-Loranet a offert son support et ses conseils pour le développement de l’application.
Le Service de l’Environnement (SEn) autre partenaire du projet Fri-LoRanet s’est offert pour calibrer les détecteurs et contrôler leur qualité. Les bus des TPF passant devant leurs stations automatiques permettront la corrélation des données mesurées sur les bus avec celles du service de l’environnement.
Le développement du système se fera en premier au labo. Dès que les mesures pourront être réalisées et transmises au serveur, un boitier sera créé pour pouvoir être installé sur une voiture de l’HEIA-FR et être alimenté par sa batterie. Les TPF ont accepté d’installer le système sur leur bus dès que les tests de la voiture seront concluants.



## Objectifs

1)	Etat de l’art des mesures de pollution mobile
2)	Récupérer par le réseau LoRa Fribourgeois les densités de particules fines (PM1, PM2.5 et PM10)
3)	Localiser le capteur en utilisant un traqueur gps muni d’une interface LoRa
4)	Installer les 2 capteurs sur le toit d’un véhicule dans un boîtier résistant aux intempéries et alimenté par la batterie du véhicule
5)	Cartographier la qualité de l’air dans la ville de Fribourg grâce aux données émises par le véhicule et la réalisation d’une application de visualisation de la pollution.



## Contraintes

Utilisation de capteurs LoRa et du réseau FRI-LoRanet, s'adapter aux besoins de la ville, suivre les conseils et règlements du service de l'environnement et des TPF
