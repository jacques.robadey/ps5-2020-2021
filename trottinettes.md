---
version: 2
titre: trottinettes connectées
type de projet: Projet de semestre 5
année scolaire: 2020/2021
mandants:
  - ville de Bulle
  - JM Fleets intl
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 2
professeurs co-superviseurs:
  - Jean-Roland Schuler
mots-clé: [LoRa versus gprs, Internet des objets, positionnement gps, mobilité douce, trottinettes connectées]
langue: [F]
confidentialité: non
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth]{img/trottinette.jpg}
\end{center}
```

## Contexte

La ville de Bulle vient de lancer un projet de location de trottinettes électriques allant jusqu’à 20 km/h. Comme cette ville possède depuis peu un tout nouveau réseau LoRa, le but de se projet est de pouvoir localiser les trottinettes par LoRa et d’accéder à leurs positions depuis son téléphone portable. La trottinette la plus proche pourrait ensuite être réservée.
Un traqueur gps devra être choisi soit munis d’une interface LoRa, soit lié à une carte LoRa via un système embarqué. Une interface de visualisation web based devra être développée avec le but d’être utilisée sur un smartphone. Un système de réservation pourra être mis en place durant le projet de semestre. Le système de localisation devra tenir dans un petit boîtier pouvant être installé sur la trottinette et sera alimenté par la batterie de la trottinette. Avant l’installation sur les trottinettes, le système pourra être testé sur des vélos privés.



## Objectifs

1)	Etat de l’art des mesures de localisation via LoRa
2)	Utiliser et tester un traqueur gps muni ou connecté à une interface LoRa
3)	Création et test d’un boîtier résistant aux intempéries et alimenté par une batterie du vélo (ou trottinette)
4)	Test sur un vélo privé et si possible sur une trottinette électrique
5)	Estimation critique de la solution en fonction des tests de couverture LoRa sur la ville de Bulle et proposition de solutions alternatives dans le cas de non fonctionnement



## Contraintes

Utilisation de traqueurs gps avec interface LoRa, utiliser le réseau FRI-LoRanet, s'adapter aux besoins de la ville
