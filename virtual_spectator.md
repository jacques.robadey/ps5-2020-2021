---
version: 2
titre: des stades remplis de spectateurs virtuels
type de projet: Projet de semestre 5
année scolaire: 2020/2021
mandants:
    - Post covid19 HEIA-FR projects
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 2
professeurs co-superviseurs:
  - Jacques Robadey
mots-clé: [Video on Demand, Sécurité, Covid19, open source, événements publiques, virtualisation]
langue: [F]
confidentialité: non
suite: non
---

## Contexte

Compétitions sportives, concerts, Théâtre sont annulées, repoussées ou ont lieu avec un nombre extrêmement limité de spectateurs, et perdent de ce fait leur rentabilité. Pour satisfaire le besoin des spectateurs et assurer le financement d’activités culturelles et sportives à grand public, des plateformes de visualisation virtuelles peuvent être organisées.
Le but de ce travail de semestre est de proposer et tester une nouvelle plateforme video (amateur), un réseau de multicasting et des accès à la demande payants pour pouvoir rendre les stades de sport et concerts à nouveau visible pour des spectateurs (virtuels) payants.



## Objectifs

De manière chronologique, le projet a les objectifs suivants :
1)	Faire l’état de l’art des bases de plateforme vidéo actuelles avec leur avantage et inconvénients tout en contrôlant qu’une utilisation de ces plateformes à but lucratif est accepté par le producteur de la plateforme (comme par exemple avec un système open source).
2)	Sélectionner ou créer un système web based pour l’enregistrement et la distribution de données video.
3)	Tester le système en labo en connectant des caméras et microphones externes avec un serveur et en récoltant les vidéos en life sur différents end-devices.
4)	Développer un système d’inscription payant au service et tester le avec si possible des clés de sécurité.
5)	Faire une démo d’un événement quelconque retransmis uniquement à des personnes qui se sont inscrites et ont payé auparavant et valider ainsi la solution.



## Contraintes

S’assurer du fonctionnement du système pour des milliers de personnes